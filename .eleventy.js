let markdownIt = require("markdown-it");
let implicitFigures = require("markdown-it-implicit-figures");
let attributes = require("markdown-it-attrs");
let bracketedSpans = require("markdown-it-bracketed-spans");
let blockEmbedPlugin = require("markdown-it-block-embed");
let html5Embed = require("markdown-it-html5-embed");
let frame = require("markdown-it-iframe");
var momentz = require("moment-timezone");

const pluginRss = require("@11ty/eleventy-plugin-rss");

module.exports = function(eleventyConfig) {
  // rss
  eleventyConfig.addPlugin(pluginRss);

  eleventyConfig.addPassthroughCopy({ "static/css": "css" });
  eleventyConfig.addPassthroughCopy({ "static/js": "js" });
  eleventyConfig.addPassthroughCopy({ "static/admin": "admin" });
  eleventyConfig.addPassthroughCopy({ "static/images": "images" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "fonts" });

  // packages for markdown
  let markdownLib = markdownIt({ html: true })
    .use(implicitFigures, {
      dataType: false, // <figure data-type="image">, default: false
      figcaption: true, // <figcaption>alternative text</figcaption>, default: false
      tabindex: true, // <figure tabindex="1+n">..., default: false
      link: true, // <a href="img.png"><img src="img.png"></a>, default: false
    })
    .use(frame)
    .use(attributes)
    .use(bracketedSpans)
    .use(blockEmbedPlugin, {
      containerClassName: "video-embed",
      outputPlayerSize: false,
    })
    .use(html5Embed, {
      html5embed: {
        useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
        useLinkSyntax: true, // Enables video/audio embed with []() syntax
      },
    });
  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter("markdownify", (value) => markdownLib.render(value));

  eleventyConfig.addFilter("replace", (value, x, y) => {
    return (value = value.replace(x, y));
  });

  // Get only content that matches a tag
  eleventyConfig.addCollection("before", function(collectionApi) {
    return collectionApi.getFilteredByTag("compte-rendu");
  });
  eleventyConfig.addCollection("next", function(collectionApi) {
    return collectionApi
      .getFilteredByTag("prochain rendez-vous")
      .sort((a, b) => {
        a.date - b.date;
      });
  });

  eleventyConfig.addFilter(
    "formatDate",
    function(value, targetTimeZone = "UTC", format, locale) {
      momentz.locale(locale);
      return momentz(value).tz(targetTimeZone).format(format);
    },
  );

  return {
    dir: {
      input: "views",
      output: "public",
      includes: "_includes",
    },
  };
};
