--- 
title: "FOAM"
subtitle: Un système personnel de gestion et de partage de connaissances
author: Arthur Perret
invite: David Benqué
portrait: '/images/davidbenque-kikk.jpg'
date: 2021-01-28
layout: cr.njk
tags: 
- compte-rendu 
- articles
visiourl: https://bigbluebutton.cloud68.co/b/jul-p0g-nnn-sl3
intro: "En partant des besoins liés à sa prise de notes, David Benqué nous présente les enjeux des outils qui suivent la méthode Zettelkasten, dont Foam est un exemple. De l'intérêt du format texte aux capacités de gestion, la discussion s'élargit rapidement à des questions de dépendances logicielles et de médiations techniques."
permalink: articles/2021-01-28-david-benque.html
customcss: foam.css
class: foam
invitebio: "[David Benqué](https://davidbenque.com/about/) est designer-chercheur, diplômé du Royal College of Art de Londres et auteur d'une [thèse](https://davidbenque.com/phd/) sur les prédictions algorithmiques. Il travaille actuellement chez XWiki sur le logiciel [CryptPad](https://cryptpad.fr), une alternative open source et encryptée à Google Docs, et poursuit ses recherches de façon indépendante sous l'appellation [*Institute of diagram studies*](https://diagram.institute)."
invitepic: davidbenque-kikk.jpg
---  

<!-- Illustration ? -->

## Prise de notes, prise de tête ?

Durant sa thèse, David s'est retrouvé confronté à la nécessité croissante de gérer sa prise de notes. Lectures, journaux de bord, réflexions spontanées… Comment inscrire et gérer tous ces fragments, notamment pour éviter de devoir redécouvrir sans cesse les mêmes idées ?

David a testé plusieurs logiciels aux profils très différents, pas toujours orientés spécifiquement vers la prise de notes (en partie à cause des spécificités du travail académique). Il a ainsi utilisé successivement [Scrivener](https://www.literatureandlatte.com/scrivener/), [Zotero](https://www.zotero.org) et [Joplin](https://joplinapp.org).

<!-- Illustration ? -->

Ce panorama relativement diversifié lui a permis de formaliser son cahier des charges personnel, tout en constatant qu'il n'existe pas d'outil parfait. Au passage, il a fini par développer un intérêt pour les systèmes de prise de notes en tant que tels.

Le principal critère de David se révèle être l'interopérabilité, en particulier l'utilisation de normes ouvertes pour l'écriture. Parmi ses points de vigilance, la question des variantes propriétaires de langages très répandus comme [Markdown](https://fr.wikipedia.org/wiki/Markdown), qui peuvent créer des effets de verrouillage passif (*soft lock*).

À cette préoccupation fondamentale s'ajoute le support de ses différentes pratiques de gestion : gestion des versions des fichiers avec [git](https://git-scm.com) ; gestion des références bibliographiques avec Zotero ; gestion centralisée des pièces jointes, notamment les images.

## Foam, ou l'écume des notes

David a trouvé la réponse à la plupart de ses besoins dans un dispositif expérimental encore en développement : l'extension [Foam](https://foambubble.github.io/foam/) pour l'éditeur de code [Visual Studio Code](https://code.visualstudio.com) de Microsoft et sa version libre [VSCodium](https://vscodium.com).

Foam est une alternative gratuite et open source à [Roam](https://roamresearch.com). Le nom est évidemment un pied-de-nez à la démarche de Roam, logiciel payant (par abonnement) au développement fermé. On retrouve ici la philosophie parfois abrasive du libre, mais au-delà de la boutade, Foam pousse la logique d'écosystème modulaire jusqu'au bout : l'extension ne fait pas tout, et recommande une liste d'autres extensions pour VSCode et VSCodium qui, combinées, lui permettent de montrer tout son potentiel.

<!-- Illustration ? -->

Mettre en œuvre Foam, c'est équiper sa prise de notes d'un certain nombre de fonctionnalités qui la transforment en un véritable système de documentation hypertextuelle : liens internes façon wiki ; rétroliens contextualisés ; représentation graphique interactive ; maintenance automatique du système.

Foam permet à David de faire fonctionner plusieurs *workflows*, ce qui est une victoire en soi : journal de bord et journal de projet ; liste de tâches ; rédaction technique, avec citations et diagrammes ; ébauches de réflexion (*stubs*), grâce à la possibilité de créer un lien vers une note qui n'existe pas encore ; etc.

Parmi les limites de Foam, David mentionne la difficulté à intégrer des besoins hétérogènes, notamment concernant la prévisualisation du texte : il lui arrive de jongler entre deux extensions, suivant qu'il ait besoin de circuler via les liens internes ou bien de voir le rendu automatique de ses citations.

Mais David mentionne également les risques inhérents à la prise de notes en général, surtout lorsqu'elle est outillée par un système comme Foam. En effet, la facilité avec laquelle on peut rapidement générer beaucoup de contenu (notes mais aussi liens) rentre en tension avec le besoin de rester productif et de contrôler sa pratique. On retrouve cette critique dans [un article](https://reallifemag.com/rank-and-file/) de *Real Life Magazine*, recommandé par David.

## Discussion

Suite à la présentation de David, nous avons ouvert plusieurs discussions.

[Antoine Fauchié](https://www.quaternum.net/a-propos/) a soulevé la question de la dépendance à un outil comme VSCode : que se passe-t-il si quelqu'un veut changer d'éditeur de texte ? Pour quelqu'un comme David, qui a expliqué habiter dans VSCode, le problème ne se pose pas. Mais pour d'autres, il faut garder à l'esprit que Foam est bâti à l'intérieur de VSCode et non par-dessus ou à côté : à moins de retrouver des fonctionnalités équivalentes ailleurs, on est lié à l'éditeur, pour le meilleur comme pour le pire.

Antoine a également ouvert le débat sur les « dispositifs de Babel », c'est-à-dire les outils qui cherchent à accueillir toutes les pratiques. Nous sommes nombreux à naviguer d'un logiciel à l'autre en fonction du type de fichier que nous modifions et des processus dans lesquels nous sommes impliqués. Nos rôles, nos humeurs et nos objets varient, et la prise en charge de cette diversité dans un environnement unique pose la question d'une homogénéisation non désirable. La remarque s'applique d'autant plus à Foam que cette extension transforme VSCode en un outil extrêmement versatile, capable d'accomoder des pratiques textuelles très différentes.

Foam propose un graphe interactif permettant de visualiser les liens entre les notes. Ce n'est pas la fonctionnalité qui a le plus convaincu David, mais c'est peut-être parce qu'elle est très peu développée. [Arthur Perret](https://www.arthurperret.fr) a suggéré que l'interface bâtie autour du graphe conditionnait largement son utilité, et que cette interface devait reposer sur des mécanismes textuels sous-jacents : utiliser un système de catégories ou de mots-clés permettrait par exemple de filtrer l'affichage du graphe sur cette base.

<!-- Illustration ? -->

Dans Foam, les liens se font sur la base des titres des notes, et l'extension met à jour les liens si les titres changent. Arthur a mentionné sa préférence pour des liens renvoyant à des identifiants uniques jamais modifiés. C'est un trait fondamental du Web (URL) et de certains systèmes de prise de notes interreliées comme la [méthode Zettelkasten](https://fr.wikipedia.org/wiki/Zettelkasten). Plusieurs systèmes de gestion des connaissances récents comme Foam ou [Obsidian](https://obsidian.md) substituent un peu vite le tout-algorithme à ces logiques documentaires plus facilement interopérables.

<!-- *Inachevé d'imprimer will return* -->
