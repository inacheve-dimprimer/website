--- 
title: "Aurore Turbiau – Litote"
author: Arthur Perret
invite: "Aurore Turbiau"
portrait: ""
date: 2024-06-11
nodate: false
layout: cr.njk
tags: 
- compte-rendu 
- articles
intro: "Aurore Turbiau est l'autrice d'une thèse sur l'engagement littéraire en lien avec le féminisme. Elle a également créé Litote, un logiciel de gestion de corpus et d'aide à la publication. Elle est venue discuter avec nous d'invention typographique, du fait de fabriquer ses propres outils, d'hypertextualité et de l'importance de la citation."
permalink: articles/2024-06-11-aurore-turbiau.html
mastercss: "litote.css"
class: paginated
invitebio: ""
invitepic: ""
---

Dans les années 1970, la littérature francophone est traversée par une vague féministe et lesbienne – entre 350 et 400 œuvres, aujourd'hui peu connues. Aurore Turbiau a travaillé pendant plusieurs années sur ce corpus, en explorant la notion d'engagement littéraire. Elle publiera prochainement un livre tiré de sa thèse. Nous avons eu le plaisir de la recevoir à Inachevé d'imprimer pour discuter notamment de Litote, son outil de recherche.

***

Les œuvres étudiées par Aurore appartiennent aux littératures expérimentales : elles travaillent l'espace de la page, mélangent des écritures typographiques et manuscrites, jouent sur les caractères, inventent et détournent des mots… Leurs autrices se sont vues accusées d'être « illisibles », tant du point de vue formel que linguistique : si leurs œuvres sont jugées immorales, inconvenantes, c'est autant pour ce qu'elles disent que pour la manière dont elles tordent les codes de la langue et de l'imprimé. Ce jugement persiste jusque dans les rétrospectives actuelles. Aurore souligne que le même qualificatif, appliqué à un homme, aurait été élogieux, car associé à la déconstruction du récit, à la mort de l'auteur…

L'approche comparatiste suivie par Aurore consiste à traiter d'un thème ou d'une question de littérature à partir de différents contextes – souvent culturels, parfois linguistiques. Au sein du corpus, on distingue les œuvres primaires (sur lesquelles s'effectue l'analyse principale) et les œuvres secondaires (qui permettent de confirmer l'analyse). Le corpus primaire est toujours plus restreint : dans le cas d'Aurore, ce sont 8 autrices sur un total de 80.

En littérature comparée, on commence à considérer qu'un corpus est grand à partir de 20 à 30 œuvres. Ici, ce sont entre 350 et 400 œuvres, un volume inhabituel qui pose quelques difficultés. Certains textes sont difficiles à trouver ; on peut y avoir accès à un moment, puis plus du tout. Surtout, il est impossible de tout mémoriser, alors que le comparatisme nécessite de pouvoir revenir régulièrement sur des détails précis. Il faut donc une méthode documentaire.

Au début de sa thèse, Aurore commence par copier des extraits d'œuvres dans un document de collecte ; très vite, le travail de description et la masse d'informations la conduisent à basculer les données dans un tableur. Mais ni traitement de texte ni tableur n'ont été conçus pour organiser et décrire des données textuelles. Aurore tente alors plusieurs autres pistes, sans trouver satisfaction : les outils permettant de gérer des matériaux de recherche, comme Zotero et Tropy, ne sont pas adaptés à la granularité très fine qu'implique l'analyse comparatiste ; les éditeurs d'hypertexte comme Obsidian n'ont pas des capacités suffisantes en classement et recherche d'information ; et les logiciels permettant de créer des bases de données sont trop complexes à prendre en main.

On pourrait penser que créer son propre outil de A à Z serait encore plus ardu. Mais après avoir acquis des bases en HTML et en PHP lors d'une formation, Aurore réalise qu'elle pourrait fabriquer une interface web pour créer et interroger une base de données. Alors elle s'y met, et au fil des mois elle développe progressivement différentes fonctionnalités adaptées à son travail : saisir un extrait d'œuvre avec des métadonnées descriptives (autrice, titre, éditeur…) ; sélectionner des extraits sur la base de mots-clés ; faire des recherches plein texte ; regrouper des extraits par paquets ; et une fonction qui permet de placer des extraits dans une arborescence, pour planifier leur utilisation dans un travail rédactionnel – en l'occurrence, une thèse.

Au terme de sa recherche, Aurore a choisi de publier la base de données avec son interface comme une annexe de la thèse. Les extraits n'excèdent pas 3000 signes et font partie d'un dispositif discursif dans lequel on voit clairement des commentaires personnels : la publication respecte ainsi le régime de la citation courte. Pour Aurore, il ne s'agissait pas seulement de respecter la déontologie scientifique : comme le dit Sara Ahmed dans *Living A Feminist Life* (2017), on peut citer des autrices et des œuvres pour contribuer à les sortir de l'invisibilité. Une membre du jury de thèse d'Aurore a relevé que 63% des travaux cités dans sa bibliographie critique ont été publiés par des femmes. L'outil et la thèse ont ainsi permis de faire entendre des voix ; cela répond à la fois à un positionnement comparatiste et à un positionnement féministe.

On peut consulter la base de données à l'adresse suivante : <https://litteratures-engagees-feminisme.huma-num.fr> Aurore nous avertit que la mise en données des œuvres et la présentation sous forme d'extraits dans une interface crée un risque : celui de traiter l'œuvre comme une collection de fiches, de perdre la littérature derrière un discours analysé. Il faut donc toujours revenir aux textes, et savoir replacer la base de données dans son contexte : c'est un instrument de recherche.

Aurore a également publié le code source de l'outil, qu'elle a appelé Litote, d'après la figure de style qui consiste à en dire le moins pour en dire le plus ; c'est aussi le nom d'un personnage dans l'une des œuvres du corpus. Ce nom symbolise le rapport entre les échelles micro et macro, le mouvement d'aller et retour entre un point de vue global et les détails, qui est le mouvement de la démarche comparatiste elle-même.

On peut utiliser Litote comme un outil d'analyse du discours mais on peut aussi le voir de façon plus abstraite comme un outil permettant de manipuler des citations. C'est d'ailleurs dans cette perspective qu'Aurore travaille en ce moment sur l'export des données depuis Litote vers les applications de *flashcards* comme Anki, pour que le logiciel puisse aider à préparer certains concours qui nécessitent d'apprendre des citations par cœur. Soulignons que, loin d'en faire un instrument de distinction individuelle, elle réfléchit plutôt à des fonctionnalités collaboratives. On peut y voir une influence de la dimension politique de son travail : dans le lien avec la communauté des « geeks » de l'enseignement supérieur et de la recherche, il y a l'engagement en faveur du logiciel libre et l'ouverture des données, mais aussi l'idée plus ambitieuse encore de construire des communs.

***

En guise de bonus, signalons que la thèse d'Aurore comporte plusieurs index, dont elle raconte la fabrication sur son carnet de recherche : <https://engagees.hypotheses.org/4186> Elle fait ici œuvre utile car les index se font de plus en plus rares dans les publications scientifiques en France, alors qu'il s'agit d'outils intellectuels essentiels.  
   
*Compte-rendu rédigé et mis en page par Arthur Perret*

