--- 
title: "Emacs / N Λ N O"
subtitle: Le design des éditeurs de texte
author: Arthur Perret
invite: Nicolas Rougier
portrait: ""
date: 2021-05-27
layout: cr.njk
tags: 
- compte-rendu 
- articles
visiourl: https://bigbluebutton.cloud68.co/b/jul-p0g-nnn-sl3
intro: "Nicolas Rougier présente le thème qu'il a créé pour Emacs et nous invite à examiner le design dominant des éditeurs de texte : ses implicites, ses travers et ses mutations possibles, avec quelques pistes d'exploration pour représenter différemment le code à l'écran."
permalink: articles/2021-05-27-nicolas-rougier.html
customcss: "nano.css"
class: nano
invitebio: "[Nicolas Rougier](https://www.labri.fr/perso/nrougier/) est chercheur en neurosciences cognitives computationnelles à l'Inria, au sein de l'Institut des maladies neurodégénératives de Bordeaux. Ses principaux intérêts en dehors des neurosciences sont : la philosophie, le libre accès aux publications scientifiques, la reproductibilité de la recherche, la visualisation de données, la médiation à destination du grand public ; mais aussi (et c'est ce qui va nous intéresser aujourd'hui) la typographie, le graphisme à l'écran et les interfaces humain-machine."
invitepic: nicolas-taffin.jpg
---

## Les éditeurs de code aujourd'hui

Nicolas a pré-publié un article intitulé [« On the design of text editors »](https://arxiv.org/abs/2008.06030) sur arXiv en 2020. Il y présente un thème qu'il a créé pour l'éditeur [Emacs](https://www.gnu.org/software/emacs/), et s'appuie sur cette expérimentation pour interroger les implicites de conception des éditeurs de texte et suggérer des pistes issues notamment du paradigme du livre.

Emacs est l'un des éditeurs historiques encore très utilisés aujourd'hui. Il est très extensible et personnalisable, sous réserve de savoir programmer en Lisp. C'est l'éditeur de prédilection de Nicolas, sur lequel il s'appuie pour son écriture mais également son agenda, ses mails, ses flux RSS…

Pour présenter Emacs et parler de son thème, Nicolas prend d'abord un exemple d'éditeur de texte moderne dont la conception est à l'opposé de ce que fait Emacs : [Nova](https://nova.app). Reprenant une capture d'écran des fonctionnalités de cet éditeur, il la transforme en un catalogue des défauts modernes des éditeurs de texte :

- l'étroitesse de la zone dédiée à la saisie (35% de la fenêtre seulement) ;
- la redondance des informations affichées ;
- l'effet « sapin de Noël » d'une coloration syntaxique trop variée et trop utilisée ;
- un long listing linéaire dans lequel on manque de repères ;
- etc.

Au-delà de cet exemple, Nicolas rappelle que la majorité des éditeurs de code modernes ([Atom](https://atom.io/), [Sublime Text](https://www.sublimetext.com), [VS Code](https://code.visualstudio.com), etc.) partagent ces défauts. Tous ont la même interface ou presque, dont beaucoup d'éléments – arborescence de fichiers, barres d'états, panneaux avec outils de développement – empiètent sur la zone de saisie. Lire et écrire du code reste la principale action qu'on cherche à effectuer dans un éditeur de code. C'est en ce sens que Nicolas a fabriqué son thème pour Emacs.

## La nano-élégance

Nicolas a apporté plusieurs modifications importantes à Emacs à travers son thème. Les plus fondamentales s'inspirent du paradigme livresque :

- le format de la fenêtre obéit à un ratio 1√2, qui gouverne les formats de papiers normés [ISO 216](https://fr.wikipedia.org/wiki/ISO_216) (comme le format A4) ;
- le contenu est encadré par des marges confortables ;
- l'interligne est augmenté pour améliorer la lisibilité.

![La page d'accueil de Emacs / N Λ N O](/images/nano-screenshot-1.png)

Ensuite, seulement six couleurs sont présentes au niveau du texte. Nicolas puise ici dans la psychologie et les neurosciences (son principal domaine d'activité) pour revoir complètement l'usage de la couleur : au lieu de faire ressortir la syntaxe sur la base de critères flous, il cible les éléments en fonction de leur [saillance](https://fr.wikipedia.org/wiki/Saillance), distinguant des zones critiques et d'autres beaucoup plus secondaires.

L'usage des polices est également modifié, avec un usage de graisses non conventionnelles (léger, moyen) par rapport aux éditeurs classiques (qui restreignent souvent le choix à normal et gras). Nicolas pioche aussi dans une plus large palette de pratiques typographiques, avec des tailles différentes pour les titres et des colonnes, et il n'hésite pas à varier le rendu en fonction de ce qui est affiché par l'éditeur (une liste de tâches, un fragment de code, un flux RSS, un article).

![Un article en cours d'édition dans Emacs / N Λ N O](/images/nano-screenshot-3.png)

Après avoir prototypé ces changements dans un thème appelé Elegant Emacs, Nicolas a publié son thème sous le nom de [Nano](https://github.com/rougier/nano-emacs) – « Emacs made simple ».


## Représenter le code à l'écran

Concernant le thème général de la représentation du code à l'écran, Nicolas mentionne plusieurs directions qu'il n'a pas explorées lui-même mais qui font l'objet de recherches chez d'autres personnes.

Au-delà de la représentation linéaire, qui reste dominante, plusieurs alternatives existent. On citera notamment la [programmation lettrée](https://fr.wikipedia.org/wiki/Programmation_lettrée) (*literate programming*), dans laquelle code et prose sont entremêlés. On peut aussi jouer sur la disposition spatiale (*layout*), en affichant par exemple les commentaires en marge du code (sans occuper plus de place) et offrir ainsi une lisibilité très différente.

Une autre piste d'expérimentation concerne les polices. Elles peuvent être utilisées pour modifier le rendu du code « source » : on peut afficher un petit F à côté d'une variable de type `float`, ou bien appliquer des ligatures qui transforment deux signes `<=` en un seul `≤`. Les polices variables ouvrent de nouvelles possibilités de variations typographiques fines au sein d'un même fichier. Des polices à chasse fixe peuvent inclure des caractères aux proportions différentes. L'apparition des lettres et le déplacement du curseur peuvent être animés de manière à fluidifier l'expérience d'écriture. Et bien d'autres possibilités existent sûrement.


## Discussion

Pour Nicolas, bien peu de choses semblent avoir changé dans le design des éditeurs de code depuis l'invention d'Emacs, d'où l'envie de réinventer, dynamiser (voire dynamiter) leur interface. La dimension esthétique est importante dans cette démarche : si Nicolas se réfère au paradigme du livre, ce n'est pas uniquement sur la base de critères rationnels mais aussi par affection et par sensibilité.

Toutefois ce n'est pas forcément simple, car Emacs est difficile à modifier, et c'est un constat qu'on peut étendre à d'autres éditeurs. Certains éléments sont particulièrement ardus : autant la coloration syntaxique est relativement simple à implémenter, autant les alternatives sémantiques peuvent devenir complexes voire irréalisables.

Par ailleurs, hors du monde du code pur et dur, des progrès ont été fait. Les éditeurs de texte destinés à un plus large public, et notamment les éditeurs Markdown, se démarquent par des efforts notables sur l'esthétique (par exemple [iA Writer](https://ia.net/writer), cité plus bas parmi les liens). Julien Taquet rappelle que les éditeurs de code sont écrits par des gens qui sont imprégnés de l'environnement Web dans lequel une page unique défile à l'infini. C'est donc peut-être en adoptant une démarche hybride entre différents milieux que peut émerger une nouvelle conception des environnements d'écriture et d'édition.


## Synthèse des liens partagés sur le chat

### Typographie

- (David Benqué) [Kitty](https://sw.kovidgoyal.net/kitty/#) et la possibilité de choisir les ligatures.
- (Nicolas Rougier) [Nerdfonts](https://www.nerdfonts.com/) pour ajouter des glyphes à des polices.
- (Nicolas Rougier) [Une synthèse](https://hal.inria.fr/hal-01815193) didactique sur le rendu des polices à l'écran.
- (Nicolas Taffin) [Un générateur ASCII](https://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20) pour créer des titres géants, à utiliser par exemple pour hacker la minimap d'un éditeur de code.
- (Arthur Perret) Les recherches typographiques de Information Architects (iA) pour leur éditeur Writer : [ici](https://ia.net/topics/in-search-of-the-perfect-writing-font) et [ici](https://ia.net/writer/blog/a-typographic-christmas).

### Couleurs

- (Damien Cassou) [Modus themes](https://gitlab.com/protesilaos/modus-themes), pensé pour assurer un niveau de contraste et avec des options pour certains handicaps visuels.
- (Nicolas Rougier) [Nord](https://www.nordtheme.com/), principale inspiration pour les couleurs de Emacs / N Λ N O.

### Outils d'écriture

- (Nicolas Rougier) [Writeroom](http://www.hogbaysoftware.com/products/writeroom).
- (Damien Cassou) [Glamorous Toolkit](https://gtoolkit.com/) et leur [blog](https://blog.feenk.com/).
- (Guillaume Brioudes) [Cold Turkey Writer](https://getcoldturkey.com/writer/).
- (Nicolas Taffin) [Soulver](https://soulver.app/), une petite appli étonnante sur le langage naturel.
- (Julien Taquet) Dans le même genre, [Calca.io](http://calca.io/).
- (Julien Taquet) [Dar](https://github.com/substance/dar), le format reproductible de la revue eLife.

### Coko

Coko organise l'Open Publishing Fest qui a donné l'impulsion à ce qui est devenu Inachevé d'imprimé. Coko fournit l'infrastructure de visio qui propulse nos rencontres. Quelques liens pour découvrir la fondation et son travail :

- [Coko](https://coko.foundation)
- [Open Publishing Fest](https://openpublishingfest.org/)
- [Open Publishing Awards](https://openpublishingawards.org/)
- [Paged.js](https://www.pagedjs.org)

***

Compte-rendu rédigé et mis en page  
par Arthur Perret, composé en  
Roboto Mono Light  
et Medium.