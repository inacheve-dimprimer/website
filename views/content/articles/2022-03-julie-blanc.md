--- 
title: "Julie Blanc"
subtitle: Créer, concevoir, fabriquer et produire des publications avec le Web
author: Antoine Fauchié
invite: Julie Blanc
portrait: ""
date: 2022-03-15
dateprecise: "Rencontre en ligne mardi 15 mars 2022 à 17h (UTC+01:00)"
nodate: false
tags: 
- compte-rendu
- articles
visiourl: https://meet.cloud68.co/user/jul-asc-p9k-azh
intro: "Julie Blanc est designer graphique, chercheuse en ergonomie et en design graphique, et développeuse. Elle travaille avec les technologies du Web, adoptant de nouvelles méthodologies et proposant de nouvelles approches pour créer, concevoir, fabriquer et produire des publications (des sites web et des livres). Cette discussion est l’occasion d’explorer trois projets dans lesquels Julie Blanc s’est impliquée."
invitebio: "[Julie Blanc](https://julie-blanc.fr/) est designer graphique, chercheuse en ergonomie et en design graphique, et développeuse."
---
